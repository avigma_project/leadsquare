﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EuroBondLeadSquaredIntegration.Models
{
    public class fgStockDTO
    {

        //public String wh_code { get; set; }
        public String ITEMSGRP_SDESC { get; set; }
        public String WAREHOUSE { get; set; }
        public String PROD_DESC { get; set; }
        public String BATCH_NO { get; set; }
        public String PROD_SUBDESC { get; set; }
        public String PROD_WIDTH { get; set; }
        public String PROD_LENGTH { get; set; }
        public String  SIZE { get; set; }
        
        public String PROD_GRADE { get; set; }
            public string SHEET { get; set; }
        public string Stock_SQM { get; set; }
    }
}