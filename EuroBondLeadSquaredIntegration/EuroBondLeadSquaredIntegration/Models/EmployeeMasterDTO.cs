﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EuroBondLeadSquaredIntegration.Models
{ 
    public class EmployeeMasterDTO
    {
      
        public string SITE_CODE { get; set; }
        public string SITE_NAME { get; set; }
        public string EMP_CODE { get; set; }
        public string EMPLOYEE_NAME { get; set; }
        public string DEPT_CODE { get; set; }
        public string DEPARTMENT { get; set; }
        public string EMP_DOJ { get; set; }
        public string EMP_PADDR1 { get; set; }
        public string EMP_PADDR2 { get; set; }
        public string EMP_PADDR3 { get; set; }
        public string LOC_CODE_PER { get; set; }
        public string EMP_CADDR1 { get; set; }
        public string EMP_CADDR2 { get; set; }
        public string EMP_CADDR3 { get; set; }
        public string LOC_CODE_CUR { get; set; }
        public string DATE_OF_JOINING { get; set; }
        public string PERMANENT_ADDRESS { get; set; }
        public string PERMANENT_CITY { get; set; }
        public string CURRENT_ADDRESS { get; set; }
        public string CURRENT_CITY { get; set; }
        public string EMP_BGROUP { get; set; }
        public string DESIGN_CODE { get; set; }
        public string DESIGNATION { get; set; }
        public string EMP_CODE_BOSS { get; set; }
        public string SUPERIOR { get; set; }
        public string EMP_STATUS { get; set; }

        public string CURRENT_ADDRESS_LOCATION { get; set; }
        public string FATHER_NAME { get; set; }
        public string EMP_NAME { get; set; }
        public string NOMINEE { get; set; }
        public string REL_DESC { get; set; }
        public string RELATION { get; set; }
        public string GRADE { get; set; }
        public string STATUS { get; set; }
        public string EMP_BASIC { get; set; }


    }
}