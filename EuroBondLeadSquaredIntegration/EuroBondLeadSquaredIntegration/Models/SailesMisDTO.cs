﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EuroBondLeadSquaredIntegration.Models
{
    public class SailesMisDTO
    {
        public string ITEMGRP_DESC { get; set; }
        public string GROUP { get; set; }
        public string ITEMSGRP_SDESC { get; set; }
        public string SUBGROUP { get; set; }
        public string ASINV_NO { get; set; }
        public string SINV_DATE { get; set; }
        public string WH_LDESC { get; set; }
        public  string  sinv_type { get; set; }
     
        public string CUST_NAME { get; set; }
        public string CUSTOMER_TYPE { get; set; }
        public string CUSTOMER { get; set; }
        public string ORD_SPLREMARKS { get; set; }
        public string PROD_GRADE { get; set; }
        public string BATCH_NO { get; set; }
        public string prod_subdesc { get; set; }
        public string ORD_EMPOTH { get; set; }
        public string EMP_CODE { get; set; }
        public string OTH_NAME { get; set; }
        public string SREG_DESC { get; set; }
        public string LOC_DESC { get; set; }
        public string  REG_LDESC { get; set; }
      
        public string CTYPE_LDESC { get; set; }
        public string MONTH { get; set; }
       public string WAREHOUSE { get; set; }
        public string TYPE { get; set; }
        public string BOOKED_BY { get; set; }
        public string STATE { get; set; }
        public string CITY { get; set; }
        public string REGION { get; set; }
        public string PRODUCT { get; set; }
        public string SHEET_QTY { get; set; }
        public string QTY { get; set; }
        public string WITHTAX { get; set; }
        public string WITHOUTTAX { get; set; }
        public string Type { get; set; }
        public string REG_CODE { get; set; }
        public string BUISNESS_REGION { get; set; }
    }
}