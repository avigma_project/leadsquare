﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EuroBondLeadSquaredIntegration.Models
{
    public class ProductMasterDTO
    {
        public string MSP_NO { get; set; }
        public string ITEMGRP_DESC { get; set; }
        public string GROUP { get; set; }
        public string ITEMSGRP_SDESC { get; set; }
        public string PROD_CODE { get; set; }
        public string PROD_ACTIVE { get; set; }
        public string UOM_CODE { get; set; }
        public string PROD_SUBDESC { get; set; }
        public string PROD_BENEFITSIZE { get; set; }
        public string PROD_LENGTH { get; set; }
        public string PROD_WIDTH { get; set; }
        public string SUBGROUP { get; set; }
        public string PRODUCT { get; set; }
        public string STATUS { get; set; }
        public string UOM_NAME { get; set; }
    }
}