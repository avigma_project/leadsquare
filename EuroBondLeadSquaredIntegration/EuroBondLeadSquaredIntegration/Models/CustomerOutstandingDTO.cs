﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EuroBondLeadSquaredIntegration.Models
{
    public class CustomerOutstandingDTO
    {
        public string AR_ZONE { get; set; }
        public string AR_REGION { get; set; }
        public string AR_CITY { get; set; }
        public string AR_STATE { get; set; }
        public string AR_CUSTTYPE { get; set; }
        public string AR_CUSTOMER { get; set; }
        public string AR_CREDIT_LIMIT { get; set; }
        public string AR_CREDIT_DAYS { get; set; }
        public string AR_INVOICE { get; set; }
        public string AR_BOOKEDBY { get; set; }
        public string AR_CUSTOMER_EMPLOYEE { get; set; }
        public string AR_SALESVOUCHER { get; set; }
        public string AR_DATE { get; set; }
        public string AR_DUE_DATE { get; set; }
    }
}