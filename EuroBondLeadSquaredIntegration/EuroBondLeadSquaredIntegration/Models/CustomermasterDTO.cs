﻿using System;

namespace EuroBondLeadSquaredIntegration.Models
{
    public class CustomermasterDTO
    {
        public string CUST_CODE { get; set; }
        public string CTYPE_LDESC { get; set; }
        public string CUST_NAME { get; set; }
        public string REG_LDESC { get; set; }
        public string BUISNESS_REGION { get; set; }
        public string GLNAME { get; set; }
        public string LOC_DESC { get; set; }
        public string CUST_ADD1 { get; set; }
        public string CUST_ADD2 { get; set; }
        public string CUST_ADD3 { get; set; }
        public string CITY { get; set; }
        public string STATE { get; set; }
        public string ADDRESS { get; set; }
        public string CUST_PANNO { get; set; }
        public string CUST_GSTIN { get; set; }
        public string CUST_EMPOTH { get; set; }
        public string EMP_CODE { get; set; }
        public string OTH_NAME { get; set; }
        public string CUST_TELNOS { get; set; }
        public string CUST_FAXNOS { get; set; }
        public string CUST_EMAILID { get; set; }
        public string CUST_WEBSITE { get; set; }
        public string CUST_CONTACTPERSON { get; set; }
        public string CUST_CONTACTDESIG { get; set; }
        public string CUST_CONTACTNOS { get; set; }
        public string CUST_PINCODE { get; set; }
        public string EMPLOYEE { get; set; }
        public string REGION { get; set; }
        public string CUST_SINCE { get; set; }
        public string CUST_VATNO { get; set; }
        public string CUST_CSTNO { get; set; }
        public string CUST_CRDAYS { get; set; }
        public string CUST_CRLIMIT { get; set; }
        public string CUST_STOPDEL { get; set; }
        public string GRADE_LDESC { get; set; }
        public string GRADE { get; set; }
        public string ACTIVE { get; set; }
        public string INACTIVE { get; set; }
        public string DATE_STAMP { get; set; }
        public string USER_ID { get; set; }
    }
}