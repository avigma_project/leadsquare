﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EuroBondLeadSquaredIntegration.Models
{
    public class RateCardDTO
    {

        public string MSP_NO { get; set; }

       // public string PROD_WIDTH { get; set; }
        public string MSP_DATE { get; set; }
//public string LOC_CODE { get; set; }
//public string CUST_CODE { get; set; }
        public string LOCATION { get; set; }
        public string CUSTOMER { get; set; }



//public string REG_LDESC { get; set; }
       // public string SREG_CODE { get; set; }
       // public string LOC_CODE_CITY { get; set; }
        public string CUST_BUISNESS_REGION { get; set; }
        public string CUST_SALESREGION { get; set; }
        public string CUSTOMER_CITY { get; set; }
        public string subgroup { get; set; }
        public string GRADE { get; set; }
        public string SITE_NAME { get; set; }
        public string SUBGROUO_RATE { get; set; }
        public string SIZE_RATE { get; set; }
        public string  PROD_SIZE { get; set; }
    }
}