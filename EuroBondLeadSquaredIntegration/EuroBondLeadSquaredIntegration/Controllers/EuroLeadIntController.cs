﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using EuroBondLeadSquaredIntegration.Repository.Data;
using EuroBondLeadSquaredIntegration.Models;

namespace EuroBondLeadSquaredIntegration.Controllers

{
    [RoutePrefix("api/EuroBondLeadSquaredIntegrationApi")]
    public class EuroLeadIntController : ApiController
    {

        

        [HttpPost]
        [Route("SailesMisData")]
        public List<SailesMisDTO> GetById()
        {
            SailesMisData sailesMisData = new SailesMisData();
            var PatitentRegistrationViewModel = sailesMisData.GetSailsMisDetails();
            return PatitentRegistrationViewModel;
        }



        [HttpGet]
        [Route("GetCustomerDetails")]
        public List<CustomermasterDTO> GetByIdCustomerDetails()
        {
            CustomerData CustomerData = new CustomerData();
            var PatitentRegistrationViewModel = CustomerData.GetCustomerDetails();
            return PatitentRegistrationViewModel;
        }



        [HttpGet]
        [Route("GetCustomerOutstandingDetails")]
        public List<CustomerOutstandingDTO> GetByIdCustomerOutstnding()
        {
            CustomerOutstandingData CustomerOutstandingData = new CustomerOutstandingData();
            var PatitentRegistrationViewModel = CustomerOutstandingData.GetCustomerOutstandingDetails();
            return PatitentRegistrationViewModel;
        }


    


        [HttpGet]
        [Route("GetEmployeeDetails")]
        public List<EmployeeMasterDTO> GetByIdEmployeeDetails()
        {
            EmployeeMasterData EmployeeMasterData = new EmployeeMasterData();
            var PatitentRegistrationViewModel = EmployeeMasterData.GetEmployeeDetails();
            return PatitentRegistrationViewModel;
        }




        [HttpGet]
        [Route("fgStockDetails")]
        public List<fgStockDTO> GetByIdfgStockDetails()
        {
            FgStockData FgStockData = new FgStockData();
            var PatitentRegistrationViewModel = FgStockData.fgStockDetails();
            return PatitentRegistrationViewModel;
        }




        [HttpGet]
        [Route("ProductMasterDetails")]
        public List<ProductMasterDTO> GetByIdProductMasterDetails()
        {
            ProductMasterData ProductMasterData = new ProductMasterData();
            var PatitentRegistrationViewModel = ProductMasterData.ProductMasterDetails();
            return PatitentRegistrationViewModel;
        }





        [HttpGet]
        [Route("RateCardDetails")]
        public List<RateCardDTO> GetByIdRateCardDetails()
        {
            RateCardData RateCardData = new RateCardData();
            var PatitentRegistrationViewModel = RateCardData.RateCardDetails();
            return PatitentRegistrationViewModel;
        }



       




    }
}
