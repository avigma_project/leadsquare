﻿using EuroBondLeadSquaredIntegration.Models;
//using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Avigma.Library;
using Oracle.ManagedDataAccess.Client;
namespace EuroBondLeadSquaredIntegration.Repository.Data
{
    public class RateCardData
    {
        Log log = new Log();

        public List<RateCardDTO> RateCardDetails()
        {
            List<RateCardDTO> list = new List<RateCardDTO>();
            string strsql = "SELECT A.MSP_NO,MSP_DATE,GETLOC(LOC_CODE)\"LOCATION\",GETCUSTOMER(a.CUST_CODE)\"CUSTOMER\",REG_LDESC\"CUST_BUISNESS_REGION\",GETREG(CC.SREG_CODE)\"CUST_SALESREGION\",   GETLOC(CC.LOC_CODE_CITY)\"CUSTOMER_CITY\",      GETSITE(SITE_CODE)\"SITE_NAME\",    ITEMSGRP_SDESC\"subgroup\",decode(b.PROD_GRADE,'X','ALL',b.PROD_GRADE)\"GRADE\",b.PROD_RATE\"SUBGROUO_RATE\",C.PROD_SIZE,C.PROD_RATE\"SIZE_RATE\"     FROM SMKT_MSP_01 A,SMKT_MSP_02 B,SMKT_MSP_03 C,INST_ITEMSUBGROUP D ,smkt_customer_01 cc,gen_regions BR   WHERE A.MSP_NO=B.MSP_NO AND B.MSP_NO=C.MSP_NO(+) and a.CUST_CODE=cc.CUST_CODE(+) AND CC.REG_CODE=BR.REG_CODE(+)   AND MSP_TYPE='C' AND B.ITEMGRP_CODE=902002    AND B.ITEMSGRP_CODE=D.ITEMSGRP_CODE AND B.ITEMGRP_CODE=D.ITEMGRP_CODE    and b.MSP_SRNO=c.MSP_SRNO(+)";
            string strfinalSQL = strsql.Replace(@"\", "");



            dbContext dbContext = new dbContext();

            using (OracleConnection conn = dbContext.GetConnection())
            {
                conn.Open();
                
                OracleCommand cmd = new OracleCommand(strfinalSQL, conn);
                try
                {
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            list.Add(new RateCardDTO()
                            {
                                MSP_NO = reader["MSP_NO"].ToString(),


                                //PROD_WIDTH = reader["PROD_WIDTH"].ToString(),
                                MSP_DATE = reader["MSP_DATE"].ToString(),
                                //LOC_CODE = reader["LOC_CODE"].ToString(),
                                //CUST_CODE = reader["CUST_CODE"].ToString(),
                                LOCATION = reader["LOCATION"].ToString(),
                                CUSTOMER = reader["CUSTOMER"].ToString(),



                                //REG_LDESC = reader["REG_LDESC"].ToString(),
                                //SREG_CODE = reader["SREG_CODE"].ToString(),
                                //LOC_CODE_CITY = reader["LOC_CODE_CITY"].ToString(),
                                CUST_BUISNESS_REGION = reader["CUST_BUISNESS_REGION"].ToString(),
                                CUST_SALESREGION = reader["CUST_SALESREGION"].ToString(),



                                CUSTOMER_CITY = reader["CUSTOMER_CITY"].ToString(),
                                subgroup = reader["subgroup"].ToString(),
                                GRADE = reader["GRADE"].ToString(),
                                SITE_NAME = reader["SITE_NAME"].ToString(),
                                SUBGROUO_RATE = reader["SUBGROUO_RATE"].ToString(),
                                SIZE_RATE = reader["SIZE_RATE"].ToString(),
                                PROD_SIZE = reader["PROD_SIZE"].ToString(),





                            });
                        }
                    }
                }
                catch (Exception ex)
                {
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    conn.Close();
                }
                finally
                {
                    conn.Close();
                }
                return list;



            }
        }


            }
}