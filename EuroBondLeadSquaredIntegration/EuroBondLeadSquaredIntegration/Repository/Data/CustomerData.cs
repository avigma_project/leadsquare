﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//using Oracle.DataAccess.Client;
//using Oracle.DataAccess.Types;
using Avigma.Library;
using EuroBondLeadSquaredIntegration.Models;
using System.Drawing;
using Oracle.ManagedDataAccess.Client;
namespace EuroBondLeadSquaredIntegration.Repository.Data
{
    public class CustomerData
    {
        Log log = new Log();
        public List<CustomermasterDTO> GetCustomerDetails()
        {
            List<CustomermasterDTO> list = new List<CustomermasterDTO>();


            string strsql = "  SELECT CUST_CODE,CTYPE_LDESC,CUST_NAME\"CUST_NAME\", REG_LDESC\"BUISNESS_REGION\",GETGL(A.GL_CODE)\"GLNAME\",   " +
                   " LL.LOC_DESC\"CITY\",(SELECT LOC_DESC FROM gen_locations WHERE LOC_CODE=LL.REF_LOC_CODE)\"STATE\",     " +
                   "(CUST_ADD1||' '||CUST_ADD2||'  '||CUST_ADD3) \"ADDRESS\",CUST_PANNO\"PAN_NUMBER\" , CUST_GSTIN,    " +
                   "DECODE(CUST_EMPOTH,'E',GETEMP(EMP_CODE),OTH_NAME)\"EMPLOYEE\"," +
                   "CUST_TELNOS,CUST_FAXNOS,CUST_EMAILID,CUST_WEBSITE,    " +
                   "CUST_CONTACTPERSON,CUST_CONTACTDESIG,CUST_CONTACTNOS, " +
                   "CUST_PINCODE,GETREG(SREG_CODE) \"REGION\",CUST_SINCE," +
                   "CUST_VATNO,CUST_CSTNO,GRADE_LDESC\"GRADE\" ,   " +
                   "CUST_CRDAYS,CUST_CRLIMIT,DECODE(CUST_STOPDEL,'Y','INACTIVE','N','ACTIVE') " +
                   "\"ACTIVE/INACTIVE\",A.USER_ID,A.DATE_STAMP   " +
                   "FROM SMKT_CUSTOMER_01 a,GEN_CUSTTYPE b ," +
                   "smkt_grade_01 c,gen_locations LL,GEN_REGIONS  ZZ  " +
                   "where a.CTYPE_CODE=b.CTYPE_CODE(+)  and CUST_GRADE=GRADE_NO(+)   " +
                   "AND A.LOC_CODE_CITY=LL.LOC_CODE (+) AND A.REG_CODE=ZZ.REG_CODE  order by 2";

                   string strfinalSQL = strsql.Replace(@"\", "");

            dbContext dbContext = new dbContext();

            using (OracleConnection conn = dbContext.GetConnection())
            {
                conn.Open();
               

                OracleCommand cmd = new OracleCommand(strfinalSQL, conn);
                try
                {
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            list.Add(new CustomermasterDTO()
                            {


                                CUST_CODE = reader["CUST_CODE"].ToString(),
                                CTYPE_LDESC = reader["CTYPE_LDESC"].ToString(),
                                CUST_NAME = reader["CUST_NAME"].ToString(),
                                //REG_LDESC = reader["REG_LDESC"].ToString(),
                                BUISNESS_REGION = reader["BUISNESS_REGION"].ToString(),
                                GLNAME = reader["GLNAME"].ToString(),
                               // LOC_DESC = reader["LOC_DESC"].ToString(),

                                // CUST_ADD1 = reader["ADDRESS"].ToString(),
                                //CUST_ADD2 = reader["CUST_ADD2"].ToString(),
                                //CUST_ADD3 = reader["CUST_ADD3"].ToString(),
                                CITY = reader["CITY"].ToString(),
                                STATE = reader["STATE"].ToString(),
                                ADDRESS = reader["ADDRESS"].ToString(),
                                CUST_PANNO = reader["PAN_NUMBER"].ToString(),
                                CUST_GSTIN = reader["CUST_GSTIN"].ToString(),
                                //  CUST_EMPOTH = reader["CUST_EMPOTH"].ToString(),



                               // EMP_CODE = reader["EMP_CODE"].ToString(),
                                //    OTH_NAME = reader["OTH_NAME"].ToString(),
                                CUST_TELNOS = reader["CUST_TELNOS"].ToString(),
                                CUST_FAXNOS = reader["CUST_FAXNOS"].ToString(),
                                CUST_EMAILID = reader["CUST_EMAILID"].ToString(),

                                CUST_WEBSITE = reader["CUST_WEBSITE"].ToString(),
                                CUST_CONTACTPERSON = reader["CUST_CONTACTPERSON"].ToString(),
                                CUST_CONTACTDESIG = reader["CUST_CONTACTDESIG"].ToString(),
                                CUST_CONTACTNOS = reader["CUST_CONTACTNOS"].ToString(),
                                CUST_PINCODE = reader["CUST_PINCODE"].ToString(),

                                EMPLOYEE = reader["EMPLOYEE"].ToString(),
                                REGION = reader["REGION"].ToString(),
                                CUST_SINCE = reader["CUST_SINCE"].ToString(),
                                CUST_VATNO = reader["CUST_VATNO"].ToString(),
                                CUST_CSTNO = reader["CUST_CSTNO"].ToString(),
                                CUST_CRDAYS = reader["CUST_CRDAYS"].ToString(),


                                CUST_CRLIMIT = reader["CUST_CRLIMIT"].ToString(),
                                //CUST_STOPDEL = reader["CUST_STOPDEL"].ToString(),
                                //  GRADE_LDESC = reader["GRADE_LDESC"].ToString(),
                                GRADE = reader["GRADE"].ToString(),
                                ACTIVE = reader["ACTIVE/INACTIVE"].ToString(),
                                //  INACTIVE = reader["INACTIVE"].ToString(),
                                //DATE_STAMP = reader["DATE_STAMP"].ToString(),
                                USER_ID = reader["USER_ID"].ToString(),


                            });
                        }
                    }
                }
                catch (Exception ex)
                {
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);

                    conn.Close();
                }
                finally
                {
                    conn.Close();
                }
                return list;

            }

        }
    }
}
