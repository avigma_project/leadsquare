﻿using EuroBondLeadSquaredIntegration.Models;
//using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Avigma.Library;
using Oracle.ManagedDataAccess.Client;
namespace EuroBondLeadSquaredIntegration.Repository.Data
{
    public class ProductMasterData
    {
        Log log = new Log();
        public List<ProductMasterDTO>ProductMasterDetails()
        {
            List<ProductMasterDTO> list = new List<ProductMasterDTO>();

            string strsql = "select ITEMGRP_DESC\"GROUP\",ITEMSGRP_SDESC\"SUBGROUP\",A.PROD_CODE,GETPRODUCT(A.PROD_CODE)\"PRODUCT\",   decode(PROD_ACTIVE,'Y','ACTIVE','I','INACTIVE')\"STATUS\",   GETUOM(A.UOM_CODE)\"UOM_NAME\",PROD_SUBDESC,PROD_BENEFITSIZE,PROD_LENGTH,PROD_WIDTH     FROM SMKT_PRODUCTMASTER A,INST_ITEMSUBGROUP B,INST_ITEMGROUP C,SMKT_PRODUCT_SIZE P      where       A.ITEMGRP_CODE=B.ITEMGRP_CODE AND A.ITEMSGRP_CODE=B.ITEMSGRP_CODE      AND A.ITEMGRP_CODE=C.ITEMGRP_CODE AND A.PROD_CODE=P.PROD_CODE     AND B.ITEMGRP_CODE=C.ITEMGRP_CODE      ORDER BY 1";
            string strfinalSQL = strsql.Replace(@"\", "");

            dbContext dbContext = new dbContext();

            using (OracleConnection conn = dbContext.GetConnection())
            {
                conn.Open();
               

                OracleCommand cmd = new OracleCommand(strfinalSQL, conn);
                try
                {
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            list.Add(new ProductMasterDTO()
                            {

                               // ITEMGRP_DESC = reader["ITEMGRP_DESC"].ToString(),
                                GROUP = reader["GROUP"].ToString(),
                                //ITEMSGRP_SDESC = reader["ITEMSGRP_SDESC"].ToString(),
                                PROD_CODE = reader["PROD_CODE"].ToString(),





                                //PROD_ACTIVE = reader["PROD_ACTIVE"].ToString(),
                                //UOM_CODE = reader["UOM_CODE"].ToString(),
                                PROD_SUBDESC = reader["PROD_SUBDESC"].ToString(),
                                PROD_BENEFITSIZE = reader["PROD_BENEFITSIZE"].ToString(),



                                PROD_LENGTH = reader["PROD_LENGTH"].ToString(),
                                PROD_WIDTH = reader["PROD_WIDTH"].ToString(),
                                SUBGROUP = reader["SUBGROUP"].ToString(),
                                PRODUCT = reader["PRODUCT"].ToString(),
                                UOM_NAME=reader["UOM_NAME"].ToString(),
                                STATUS = reader["STATUS"].ToString(),






                            });
                        }
                    }
                }
                catch (Exception ex)
                {
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    conn.Close();
                }
                finally
                {
                    conn.Close();
                }
                return list;
            }
        }
    }
}