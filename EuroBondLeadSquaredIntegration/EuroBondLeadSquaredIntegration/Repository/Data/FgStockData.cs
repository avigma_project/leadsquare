﻿using EuroBondLeadSquaredIntegration.Models;
//using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Avigma.Library;
using Oracle.ManagedDataAccess.Client;
namespace EuroBondLeadSquaredIntegration.Repository.Data
{
    public class FgStockData
    {
        Log log = new Log();
        public List<fgStockDTO> fgStockDetails()
        {
            List<fgStockDTO> list = new List<fgStockDTO>();

            string strsql = "SELECT getwarehouse(a.wh_code) warehouse,c.itemsgrp_Sdesc,b.prod_desc, A.BATCH_NO, PROD_SUBDESC,A.PROD_WIDTH,A.PROD_LENGTH, DECODE(D.PROD_BENEFITSIZE,'Y',PROD_SUBDESC,'STANDARD')\"SIZE\",a.PROD_GRADE,   sum(decode(stock_type,'R',                decode(d.prod_benefitsize, 'Y', item_qty, item_qty),              decode( d.prod_benefitsize, 'Y',item_qty * -1, item_qty * -1)))\"sheet\",  sum(decode(stock_type,'R',                decode(d.prod_benefitsize, 'Y', a.prod_width * a.prod_length * item_qty, d.prod_width * d.prod_length * item_qty),              decode(d.prod_benefitsize, 'Y', a.prod_width * a.prod_length * item_qty * -1, d.prod_width * d.prod_length * item_qty * -1))) Stock_SQM   FROM smkt_stockdetails_01 a, smkt_productmaster b, inst_itemsubgroup c, smkt_product_size d   where a.prod_code = b.prod_code   and b.itemgrp_code = c.itemgrp_code  AND c.itemgrp_code = 902002    and b.itemsgrp_code = c.itemsgrp_code    and b.prod_code = d.prod_code   and a.prod_subcode = d.prod_subcode and TRANS_DATE <='31-DEC-2013'   group by b.prod_desc,c.itemSgrp_Sdesc, getwarehouse(a.wh_code) ,A.BATCH_NO,PROD_SUBDESC,A.PROD_WIDTH,A.PROD_LENGTH,a.PROD_GRADE,  DECODE(D.PROD_BENEFITSIZE,'Y',PROD_SUBDESC,'STANDARD')  HAVING sum(decode(stock_type,'R',                decode(d.prod_benefitsize, 'Y', a.prod_width * a.prod_length * item_qty, d.prod_width * d.prod_length * item_qty),              decode(d.prod_benefitsize, 'Y', a.prod_width * a.prod_length * item_qty * -1, d.prod_width * d.prod_length * item_qty * -1)))>0  order by 1, 2, 3, 4  ";

            string strfinalSQL = strsql.Replace(@"\", "");


            dbContext dbContext = new dbContext();

            using (OracleConnection conn = dbContext.GetConnection())
            {
                conn.Open();

               

                OracleCommand cmd = new OracleCommand(strfinalSQL, conn);
                try
                {
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            list.Add(new fgStockDTO()
                            {
                                WAREHOUSE = reader["warehouse"].ToString(),
                                ITEMSGRP_SDESC = reader["itemsgrp_Sdesc"].ToString(),
                                PROD_DESC = reader["prod_desc"].ToString(),
                                BATCH_NO = reader["BATCH_NO"].ToString(),


                                PROD_SUBDESC = reader["PROD_SUBDESC"].ToString(),
                                PROD_WIDTH = reader["PROD_WIDTH"].ToString(),
                                PROD_LENGTH = reader["PROD_LENGTH"].ToString(),
                                SIZE = reader["SIZE"].ToString(),


                                PROD_GRADE = reader["PROD_GRADE"].ToString(),
                                SHEET = reader["sheet"].ToString(),
                                Stock_SQM = reader["Stock_SQM"].ToString(),



                            });
                        }
                    }
                }
                catch (Exception ex)
                {

                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    conn.Close();
                }
                finally
                {
                    conn.Close();
                }
                return list;

            }
        }
    }
}