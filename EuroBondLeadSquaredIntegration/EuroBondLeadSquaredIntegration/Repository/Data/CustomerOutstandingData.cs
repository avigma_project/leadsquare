﻿using EuroBondLeadSquaredIntegration.Models;
//using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Avigma.Library;
using Oracle.ManagedDataAccess.Client;
namespace EuroBondLeadSquaredIntegration.Repository.Data
{
    public class CustomerOutstandingData
    {
        Log log = new Log();

        public List<CustomerOutstandingDTO> GetCustomerOutstandingDetails()
        {
            List<CustomerOutstandingDTO> list = new List<CustomerOutstandingDTO>();



            string strsql = "Select P.Zone\"AR_ZONE\", P.Region\"AR_REGION\",P.CITY11\"AR_CITY\",P.STATE11\"AR_STATE\", P.CustomerType\"AR_CUSTTYPE\", P.Customer\"AR_CUSTOMER\",  P.CreditLimit\"AR_CREDIT_LIMIT\", P.CreditDays\"AR_CREDIT_DAYS\", P.INVOICE\"AR_INVOICE\",BOOKEDBY\"AR_BOOKEDBY\",CUSTOMER_EMPLOYEE\"AR_CUSTOMER_EMPLOYEE\",    P.ADOCUMENT_NUMBER\"AR_SALESVOUCHER\",to_char(P.DATE1,'DD-MON-YYYY')\"AR_DATE\",TO_CHAR(P.DUE_DATE1,'DD-MON-YYYY')\"AR_DUE_DATE\",    NVL((CASE WHEN P.DUE_DATE1<SYSDATE THEN 'OVER DUE'           WHEN P.DUE_DATE1=SYSDATE THEN 'TODAY DUE'           WHEN P.DUE_DATE1>SYSDATE THEN 'UNDER DUE'           END),'OVER DUE')\"AR_STATUS\",P.age,     Sum(Case     When P.age Between 0 And 30 Then P.balance Else 0 End) As \"AR_Age 0 To 30\", Sum(Case     When P.age Between 31 And 60 Then P.balance Else 0 End) As \"AR_Age 31 To 60\", Sum(Case     When P.age Between 61 And 90 Then P.balance Else 0 End) As \"AR_Age 61 To 90\", Sum(Case     When P.age Between 91 And 120 Then P.balance Else 0   End) As \"AR_Age 91 To 120\", Sum(Case When P.age Between 121 And 150 Then P.balance     Else 0 End) As \"AR_Age 121 To 150\", Sum(Case     When P.age Between 151 And 180 Then P.balance Else 0   End) As \"AR_Age 151 To 180\", Sum(Case When P.age > 180 Then P.balance Else 0   End) As \"AR_Age 181+\" From (Select a.DEBIT_AMOUNT - Case         When (Select Sum(fiac_slglbalances_03.CREDIT_AMOUNT)         From fiac_slglbalances_03         Where fiac_slglbalances_03.ENTRY_ID In (Select b.ENTRY_ID           From fiac_slglbalances_02 b           Where b.ENTRY_ID = a.ENTRY_ID)) Is Null Then 0         Else (Select Sum(fiac_slglbalances_03.CREDIT_AMOUNT)         From fiac_slglbalances_03         Where fiac_slglbalances_03.ENTRY_ID In (Select b.ENTRY_ID           From fiac_slglbalances_02 b Where b.ENTRY_ID = a.ENTRY_ID))       End As balance, To_Date(sysdate) - a.DOCUMENT_DATE As       age, D.SL_DESC As Customer, C.ADOCUMENT_NUMBER,C.DOCUMENT_DATE\"DATE1\",(C.DOCUMENT_DATE+(NVL(E.CUST_CRDAYS,0)))\"DUE_DATE1\",       E.CUST_CRLIMIT As       CreditLimit, E.CUST_CRDAYS As CreditDays,GETLOC(E.LOC_CODE_CITY)\"CITY11\",GETLOC(ST.REF_LOC_CODE)\"STATE11\",       Concat(Concat(Concat(Concat(E.CUST_ADD1, ' '), E.CUST_ADD2), ' '),       E.CUST_ADD3) As Address, getreg(E.SREG_CODE) As Region, F.CTYPE_LDESC As       CustomerType, G.REG_LDESC As Zone, SI.ASINV_NO\"INVOICE\",      DECODE(SO.ORD_EMPOTH,'E',GETEMP(SO.EMP_CODE),'O',SO.OTH_NAME)\"BOOKEDBY\" ,      DECODE(E.CUST_EMPOTH,'E',GETEMP(SO.EMP_CODE),'O',SO.OTH_NAME)\"CUSTOMER_EMPLOYEE\" ,      (Select Sum(fiac_slglbalances_02.CREDIT_AMOUNT) From fiac_slglbalances_02       Where fiac_slglbalances_02.CREDIT_AMOUNT > 0 And         fiac_slglbalances_02.DOCUMENT_NO Not In (Select           fiac_slglbalances_03.ADOCUMENT_NO         From fiac_slglbalances_03) And fiac_slglbalances_02.SL_TYPE = 'C' And         fiac_slglbalances_02.SL_CODE = a.SL_CODE) As UnlinkedAmount     From fiac_slglbalances_02 a ,       fiac_documents_01 C ,       SMKT_SALESINVOICE_01 SI ,       SMKT_DELIVERYCHL_01 DC ,       SMKT_SALESORDER_01 SO ,       fiac_slcodes D ,       smkt_customer_01 E ,       gen_custtype F ,       gen_LOCATIONS ST ,       gen_regions G       Where       a.DOCUMENT_NO = C.DOCUMENT_NUMBER(+) AND  E.CUST_CODE='902SD04452' AND       C.DOCUMENT_NUMBER=SI.DOCUMENT_NUMBER(+) AND       SI.DELCHL_NO=DC.DELCHL_NO(+) AND       DC.ORD_NO=SO.ORD_NO(+) AND       a.SL_CODE = D.SL_CODE AND  A.SL_CODE='902SD04452' AND       a.SL_CODE = E.CUST_CODE(+) AND       E.CTYPE_CODE = F.CTYPE_CODE(+) AND       E.LOC_CODE_CITY = ST.LOC_CODE AND       E.REG_CODE = G.REG_CODE AND       a.SL_TYPE = 'C' and D.SL_TYPE='C' AND E.IS_INACTIVE='N') P Where P.balance > 0   Group By P.Zone, P.Region,BOOKEDBY,CUSTOMER_EMPLOYEE, P.CustomerType, P.Customer,P.age, P.ADOCUMENT_NUMBER,P.CITY11,P.STATE11,P.CreditLimit, P.CreditDays ,P.INVOICE ,P.DATE1,P.DUE_DATE1 Order By P.Customer";

            string strfinalSQL = strsql.Replace(@"\", "");


            dbContext dbContext = new dbContext();


            using (OracleConnection conn = dbContext.GetConnection())
            {
                conn.Open();

                OracleCommand cmd = new OracleCommand(strfinalSQL, conn);
                
                try
                {
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            list.Add(new CustomerOutstandingDTO()
                            {


                                AR_ZONE = reader["AR_ZONE"].ToString(),

                                AR_REGION = reader["AR_REGION"].ToString(),

                                AR_CITY = reader["AR_CITY"].ToString(),

                                AR_STATE = reader["AR_STATE"].ToString(),
                                AR_CUSTTYPE = reader["AR_CUSTTYPE"].ToString(),
                                AR_CUSTOMER = reader["AR_CUSTOMER"].ToString(),
                                AR_CREDIT_LIMIT = reader["AR_CREDIT_LIMIT"].ToString(),
                                AR_CREDIT_DAYS = reader["AR_CREDIT_DAYS"].ToString(),


                                AR_INVOICE = reader["AR_INVOICE"].ToString(),
                                AR_BOOKEDBY = reader["AR_BOOKEDBY"].ToString(),
                                AR_CUSTOMER_EMPLOYEE = reader["AR_CUSTOMER_EMPLOYEE"].ToString(),
                                AR_SALESVOUCHER = reader["AR_SALESVOUCHER"].ToString(),

                                AR_DATE = reader["AR_DATE"].ToString(),
                                AR_DUE_DATE = reader["AR_DUE_DATE"].ToString(),
                                
                            });
                        }
                    }
                }
                catch (Exception ex)
                {
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    conn.Close();
                }
                finally
                {
                    conn.Close();
                }
                return list;

            }

        }
    }
}
















            
