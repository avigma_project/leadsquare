﻿using EuroBondLeadSquaredIntegration.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Avigma.Library;
//using Oracle.DataAccess.Client;
using Oracle.ManagedDataAccess.Client;
namespace EuroBondLeadSquaredIntegration.Repository.Data
{
    public class EmployeeMasterData
    {
        Log log = new Log();
        public List<EmployeeMasterDTO> GetEmployeeDetails()
        {
            List<EmployeeMasterDTO> list = new List<EmployeeMasterDTO>();
            string strsql = "SELECT GETSITE(A.SITE_CODE)\"SITE_NAME\",A.EMP_CODE,GETEMP(A.EMP_CODE)\"EMPLOYEE_NAME\",GETDEPT(DEPT_CODE)\"DEPARTMENT\",B.EMP_DOJ\"DATE_OF_JOINING\", (EMP_PADDR1||EMP_PADDR2||EMP_PADDR3)\"PERMANENT_ADDRESS\",GETLOC(LOC_CODE_PER)\"PERMANENT_CITY\", (EMP_CADDR1||EMP_CADDR2||EMP_CADDR3)\"CURRENT_ADDRESS\",GETLOC(LOC_CODE_CUR)\"CURRENT_CITY\",EMP_BGROUP\"BLOOD_GROUP\",     EMP_BASIC,GETLOC(LOC_CODE_CUR)\"CURRENT_ADDRESS_LOCATION\",GRADE_DESC\"GRADE\",EMP_FTHNAME\"FATHER_NAME\",EMP_NAME\"NOMINEE\",REL_DESC\"RELATION\",     GETDESG(DESIGN_CODE)\"DESIGNATION\",GETEMP(EMP_CODE_BOSS)\"SUPERIOR\",     DECODE(EMP_STATUS,'P','PERMANENT','C','CONTRACT','T','TEMPORARY')\"STATUS\"     FROM PAHR_EMP_01 A,PAHR_EMP_02 B,PAHR_EMP_04 C,PAHR_GRADE G,GEN_RELATIONS GR     WHERE  A.EMP_CODE=B.EMP_CODE  AND A.EMP_CODE=C.EMP_CODE(+)       AND B.GRADE_CODE=G.GRADE_CODE AND C.REL_CODE=GR.REL_CODE(+) --AND GETEMP(A.EMP_CODE) LIKE '%Nir%'     AND EMP_STATUS<>'I'I%'";



            string strfinalSQL = strsql.Replace(@"\", "");



            dbContext dbContext = new dbContext();

            using (OracleConnection conn = dbContext.GetConnection())
            {
                conn.Open();

               

                OracleCommand cmd = new OracleCommand(strfinalSQL, conn);
                try
                {
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            list.Add(new EmployeeMasterDTO()
                            {

                                //SITE_CODE = reader["SITE_CODE"].ToString(),
                                SITE_NAME = reader["SITE_NAME"].ToString(),
                                EMP_CODE = reader["EMP_CODE"].ToString(),
                                EMPLOYEE_NAME = reader["EMPLOYEE_NAME"].ToString(),
                               // DEPT_CODE = reader["DEPT_CODE"].ToString(),



                               DEPARTMENT = reader["DEPARTMENT"].ToString(),
                               // EMP_DOJ = reader["EMP_DOJ"].ToString(),
                                //EMP_PADDR1 = reader["EMP_PADDR1"].ToString(),
                                //EMP_PADDR2 = reader["EMP_PADDR2"].ToString(),



                                //EMP_PADDR3 = reader["EMP_PADDR3"].ToString(),
                                //LOC_CODE_PER = reader["LOC_CODE_PER"].ToString(),
                                //EMP_CADDR1 = reader["EMP_CADDR1"].ToString(),
                                //EMP_CADDR2 = reader["EMP_CADDR2"].ToString(),


                                //EMP_CADDR3 = reader["EMP_CADDR3"].ToString(),
                               // LOC_CODE_CUR = reader["LOC_CODE_CUR"].ToString(),
                                DATE_OF_JOINING = reader["DATE_OF_JOINING"].ToString(),
                                PERMANENT_ADDRESS = reader["PERMANENT_ADDRESS"].ToString(),


                                PERMANENT_CITY = reader["PERMANENT_CITY"].ToString(),
                                CURRENT_ADDRESS = reader["CURRENT_ADDRESS"].ToString(),
                                CURRENT_CITY = reader["CURRENT_CITY"].ToString(),
                                // EMP_BGROUP = reader["EMP_BGROUP"].ToString(),
                                //  DESIGN_CODE = reader["DESIGN_CODE"].ToString(),


                                DESIGNATION = reader["DESIGNATION"].ToString(),
                                ////EMP_CODE_BOSS = reader["EMP_CODE_BOSS"].ToString(),
                                SUPERIOR = reader["SUPERIOR"].ToString(),
                                STATUS = reader["STATUS"].ToString(),
                                CURRENT_ADDRESS_LOCATION = reader["CURRENT_ADDRESS_LOCATION"].ToString(),
                                FATHER_NAME = reader["FATHER_NAME"].ToString(),
                                // EMP_NAME = reader["EMP_NAME"].ToString(),
                                NOMINEE = reader["NOMINEE"].ToString(),

                                //// REL_DESC = reader["REL_DESC"].ToString(),
                                //RELATION = reader["RELATION"].ToString(),
                                //GRADE = reader["GRADE"].ToString(),
                                //EMP_BASIC = reader["EMP_BASIC"].ToString(),

                            });
                        }
                    }
                }
                catch (Exception ex)
                {
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    conn.Close();
                }
                finally
                {
                    conn.Close();
                }
                return list;
            }
        }
    }
}