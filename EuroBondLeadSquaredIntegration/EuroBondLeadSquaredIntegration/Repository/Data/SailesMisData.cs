﻿using EuroBondLeadSquaredIntegration.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Avigma.Library;
//using Oracle.DataAccess.Client;
using Oracle.ManagedDataAccess.Client;
namespace EuroBondLeadSquaredIntegration.Repository.Data
{
    public class SailesMisData
    {
        Log log = new Log();

        public List<SailesMisDTO> GetSailsMisDetails()
        {
            List<SailesMisDTO> list = new List<SailesMisDTO>();


            string strsql = "SELECT GP.ITEMGRP_DESC\"GROUP\",m.ITEMSGRP_SDESC\"SUBGROUP\", A.ASINV_NO,TO_CHAR(A.SINV_DATE,'MON-YY')\"MONTH\",  WH.WH_LDESC\"WAREHOUSE\",A.sinv_type,decode(A.sinv_type,'G','GST_LOCAL','S','STOCKTRANSFER','I','GST_INTERSTATE','E','EXPORTBILL','O','OTHER')\"TYPE\",  A.SINV_DATE,CD.CUST_NAME\"CUSTOMER\",GG.CTYPE_LDESC\"CUSTOMER_TYPE\",K.ORD_SPLREMARKS,  B.PROD_GRADE,B.BATCH_NO,   E.prod_subdesc,DECODE(K.ORD_EMPOTH,'E',GETEMP(K.EMP_CODE),'O',K.OTH_NAME)\"BOOKED_BY\" ,  Q.SREG_DESC\"REGION\",LL.LOC_DESC\"CITY\",  (SELECT LOC_DESC FROM gen_locations WHERE LOC_CODE=LL.REF_LOC_CODE)\"STATE\",  (SELECT REG_LDESC FROM gen_locations A,GEN_REGIONS B WHERE A.LOC_CODE=LL.REF_LOC_CODE AND A.REG_CODE=B.REG_CODE)\"BUISNESS_REGION\", D.Prod_Desc\"PRODUCT\",      SUM(b.ITEM_QTY)\"SHEET_QTY\",     SUM(DECODE(D.Prod_Subproducts,'Y',b.PROD_WIDTH * b.PROD_LENGTH * b.ITEM_QTY, b.ITEM_QTY))\"QTY\",    SUM(DECODE(D.Prod_Subproducts,'Y', ((b.PROD_WIDTH * b.PROD_LENGTH) * b.ITEM_QTY) * b.ITEM_RATE, b.ITEM_QTY * b.ITEM_RATE))\"WITHTAX\",        SUM(DECODE(D.Prod_Subproducts,'Y', ((b.PROD_WIDTH * b.PROD_LENGTH) * b.ITEM_QTY) * (b.ITEM_RATE - NVL(B.PROD_TAX_RATE,0)),                                            b.ITEM_QTY * (b.ITEM_RATE - NVL(b.PROD_TAX_RATE,0))))\"WITHOUTTAX\"                                                                   FROM SMKT_SALESINVOICE_01 A, SMKT_DELIVERYCHL_01 J, SMKT_SALESORDER_01 K, SMKT_STOCKDETAILS_01 B,             SMKT_PRODUCTMASTER D, SMKT_PRODUCT_SIZE E, INST_ITEMSUBGROUP M, SMKT_SALESREGION_01 q,            " +
                "SMKT_CUSTOMER_01 CD,GEN_CUSTTYPE GG    ,GEN_REGIONS Z,GEN_LOCATIONS LL,INST_ITEMGROUP GP,GEN_WAREHOUSE WH WHERE a.DELCHL_NO = j.DELCHL_NO  AND j.ord_no = k.ord_no AND Q.REG_CODE=Z.REG_CODE(+) AND Q.LOC_CODE=LL.LOC_CODE " +
                "AND B.TRANS_NO =  j.DELCHL_NO AND A.CUST_CODE=CD.CUST_CODE AND CD.CTYPE_CODE=GG.CTYPE_CODE AND b.TRANS_TYPE = 'DCH'   AND J.WH_CODE=WH.WH_CODE " +
                "AND D.Prod_Code = b.Prod_Code AND B.Prod_Code = E.Prod_Code(+) AND B.PROD_SUBCODE = E.Prod_Subcode(+) AND M.ITEMGRP_CODE = GP.ITEMGRP_CODE AND " +
                "M.ITEMGRP_CODE = d.ITEMGRP_CODE AND M.ITEMSGRP_CODE = d.ITEMSGRP_CODE AND GP.ITEMGRP_CODE = d.ITEMGRP_CODE AND K.ORD_TYPE != 'S'AND q.sreg_code = k.sreg_code " +
                "AND A.SINV_AUTHFLAG ='Y'AND A.SINV_DATE BETWEEN '01-JAN-2019' AND '28-FEB-2019' AND GP.ITEMGRP_CODE=902002 " +
                "GROUP BY GP.ITEMGRP_DESC,ITEMSGRP_SDESC, D.Prod_Desc,A.ASINV_NO,B.PROD_GRADE,B.BATCH_NO,Q.SREG_DESC,Z.REG_LDESC, D.Prod_Desc,  E.prod_subdesc,DECODE(K.ORD_EMPOTH,'E',GETEMP(K.EMP_CODE),'O',K.OTH_NAME),LL.LOC_DESC, WH.WH_LDESC,A.sinv_type,A.SINV_DATE,CD.CUST_NAME,GG.CTYPE_LDESC,K.ORD_SPLREMARKS, REF_LOC_CODE ORDER BY 3 ";
            string strfinalSQL = strsql.Replace(@"\", "");

            dbContext dbContext = new dbContext();

            using (OracleConnection conn = dbContext.GetConnection())
            {
                conn.Open();

                OracleCommand cmd = new OracleCommand(strfinalSQL, conn);
                try
                {
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            list.Add(new SailesMisDTO()
                            {

                                //ITEMGRP_DESC = reader["ITEMGRP_DESC"].ToString(),
                                GROUP = reader["GROUP"].ToString(),
                               // ITEMSGRP_SDESC = reader["ITEMSGRP_SDESC"].ToString(),
                                SUBGROUP = reader["SUBGROUP"].ToString(),
                                ASINV_NO = reader["ASINV_NO"].ToString(),

                                SINV_DATE = reader["SINV_DATE"].ToString(),
                              //  WH_LDESC = reader["WH_LDESC"].ToString(),
                                sinv_type = reader["sinv_type"].ToString(),
                                CUST_NAME = reader["CUST_NAME"].ToString(),
                                CUSTOMER = reader["CUSTOMER"].ToString(),
                                CUSTOMER_TYPE = reader["CUSTOMER_TYPE"].ToString(),



                                ORD_SPLREMARKS = reader["ORD_SPLREMARKS"].ToString(),
                                PROD_GRADE = reader["PROD_GRADE"].ToString(),
                                BATCH_NO = reader["BATCH_NO"].ToString(),
                                prod_subdesc = reader["prod_subdesc"].ToString(),
                              //  ORD_EMPOTH = reader["ORD_EMPOTH"].ToString(),
                              //  EMP_CODE = reader["EMP_CODE"].ToString(),


                               // OTH_NAME = reader["OTH_NAME"].ToString(),
                              //  SREG_DESC = reader["SREG_DESC"].ToString(),
                               // LOC_DESC = reader["LOC_DESC"].ToString(),
                               // REG_LDESC = reader["REG_LDESC"].ToString(),
                              //  CTYPE_LDESC = reader["CTYPE_LDESC"].ToString(),
                                CITY = reader["CITY"].ToString(),
                                BUISNESS_REGION=reader["BUISNESS_REGION"].ToString(),




                              //  MONTH = reader["MONTH"].ToString(),
                                WAREHOUSE = reader["WAREHOUSE"].ToString(),
                                TYPE = reader["TYPE"].ToString(),
                                BOOKED_BY = reader["BOOKED_BY"].ToString(),
                                STATE = reader["STATE"].ToString(),

                               REGION = reader["REGION"].ToString(),
                                PRODUCT = reader["PRODUCT"].ToString(),
                                SHEET_QTY = reader["SHEET_QTY"].ToString(),
                                QTY = reader["QTY"].ToString(),
                                WITHTAX = reader["WITHTAX"].ToString(),




                                WITHOUTTAX = reader["WITHOUTTAX"].ToString(),
                               // Type = reader["Type"].ToString(),
                               // REG_CODE = reader["REG_CODE"].ToString(),
                           
                            });
                        }
                    }
                }
                catch (Exception ex)
                {
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    conn.Close();
                }
                finally
                {
                    conn.Close();
                }
                return list;


            }
        }
    }
}
