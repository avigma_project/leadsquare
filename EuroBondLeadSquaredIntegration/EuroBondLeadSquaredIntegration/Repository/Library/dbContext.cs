﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
//using Oracle.DataAccess.Client;
using System.Configuration;
using Oracle.ManagedDataAccess.Client;

namespace Avigma.Library
{
    public class dbContext
    {

        public string ConnectionString { get; set; }

        public dbContext()
        {
            this.ConnectionString = ConfigurationManager.ConnectionStrings["OracleDbContext"].ToString();
        }

        public dbContext(string connectionString)
        {
            this.ConnectionString = connectionString;
        }

        public  OracleConnection GetConnection()
        {
            try
            {
                return new OracleConnection(ConnectionString);

            }
            catch (Exception ex)
            {


                return  null;
            }
        }

    
    }
}
